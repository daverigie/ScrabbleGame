# -*- coding: utf-8 -*-
"""
Created on Thu May 25 20:58:46 2017

@author: daverigie
"""

import consoleview as viewer
from enum import Enum
import copy
import json
import dictionary
import random

GAME_GRID_SIZE  = 15;
EMPTY_TILE_CHAR = '-'
LETTER_DATA_PATH = 'assets/letters.json'
MULTIPLIERS_PATH = 'assets/multipliers.txt'
MULTIPLIER_CHAR_CODE_PATH = 'assets/multiplier_char_code.json'
ALPHABET         = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

with open(LETTER_DATA_PATH) as letterFile:
    letterData = json.load(letterFile)

with open(MULTIPLIER_CHAR_CODE_PATH) as charCodeFile:
    multiplierCharCode = json.load(charCodeFile)    

with open(MULTIPLIERS_PATH) as f:
    multipliersText = f.read().split('\n')
    
multipliersText = [list(line) for line in multipliersText]


class GameBoard:
   
    tileGrid   = []
    
    def __init__(self):
        
        self.tileGrid = [[EMPTY_TILE_CHAR for i in range(GAME_GRID_SIZE)] for i in range(GAME_GRID_SIZE)]

    def isInBounds(self, row, col):

        nRows = len(self.tileGrid)
        nCols = len(self.tileGrid[0])

        if (row > (nRows-1)) or (col > (nCols-1)):  
            return False

        return True

    def isempty(self, row, col):

        if self.tileGrid[row][col] == EMPTY_TILE_CHAR:
            return True

        return False

    def containsLetter(self, row, col):

        if self.isInBounds(row, col) and not(self.isempty(row,col)):
            return True;

        return False;

    def addTile(self, row, col, letter):
    
        errorCode = 0 # No Error

        assert isletter(letter)

        if not(self.isInBounds(row, col)):
            errorCode = 1 # Out of Bounds
            return False, errorCode

        if  self.isempty(row,col):
                self.tileGrid[row][col] = letter.upper()
                return True, 'success'
        
        errorCode = 2 # Spot not Empty

        return False, errorCode
    
    def addWord(self, row, col, word, direction):

        assert word.isalpha()

        wordLength = len(word)
        
        for i in range(wordLength):
            
            success, errorCode = self.addTile(row, col, word[i])
            
            if not(success):
                return False, errorCode

            if direction == 'RIGHT':
                col = col + 1
            elif direction == 'DOWN':
                row = row + 1
            else:
                raise ValueError("direction must be either RIGHT or DOWN")

    def findAllWords(self):

        nRows = len(self.tileGrid)
        nCols = len(self.tileGrid[0])
    
        foundWords = []

        for row in range(nRows):
            for col in range(nCols):
                
                # Does current square contain a letter?
                if not(self.containsLetter(row, col)):
                    continue

                # Does it begin a vertical word?
                if not(self.containsLetter(row-1,col)) and self.containsLetter(row+1, col):
                    # Gather vertical word
                    word = ''
                    (i, j) = (row, col)
                    while self.containsLetter(i, j):
                        word += self.tileGrid[i][j]
                        i += 1

                    foundWords.append((row, col, word))

                if not(self.containsLetter(row,col-1)) and self.containsLetter(row, col+1):
                    # Gather horizontal word
                    word = ''
                    (i, j) = (row, col)
                    while self.containsLetter(i, j):
                        word += self.tileGrid[i][j]
                        j += 1

                    foundWords.append((row, col, word))

        return foundWords
               
        
    def getMultiplier(self, row, col):
        s = multipliersText[row][col]
        return multiplierCharCode[s]
        
    def view(self):
        viewer.view(self)

class Player:
    
    userName = ''
    tileRack = []
    score    = 0
    
    def __init__(self, userName):
        self.userName = userName
        self.tileRack = []
        self.score    = 0

class TileBag:
    
    letters = []
    
    def __init__(self):
        
        for l in ALPHABET:
            letterDict = letterData['letters'][l]
            nTiles = letterDict['tiles']
            self.letters.extend([l]*nTiles)
        
        self.shuffle()
        
    def pop(self, nTiles):
        
        nTiles = min(len(self.letters), nTiles)
        
        tiles = [self.letters.pop() for i in range(nTiles)]
        return tiles
        
    def add(self, letters):
        self.letters.extend(list(letters.upper()))
        self.shuffle()
        
    def exchange(self, letters):
        
        tiles = self.pop(len(letters))
        self.add(letters)
        return tiles
        
    def shuffle(self):
        random.shuffle(self.letters)
            
    
def isletter(letter):
    return (letter.isalpha()) and (len(letter)==1)


def getPointValue(word):

    val = 0
    for letter in word.upper():
        if isletter(letter):
            val = val + letterData['letters'][letter]['points']


def isValidPlay(fragmentList):
    
    validFlag = all((dictionary.findWord(fragment) >= 0) for fragment in fragmentList)
    
    return validFlag
        
        
        
        
        
        