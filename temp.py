import game
import json
import random
import dictionary

G = game.GameBoard()
print(G.tileGrid)
G.addWord(11,5,"SOMETHING","RIGHT")
G.addWord(10,6,"SOMETHING","RIGHT")
G.addWord(1,6,"HARPER","DOWN")

for i in range(35):
	row = random.randint(0,14)
	col = random.randint(0,14)
	direction = random.choice(['RIGHT','DOWN'])
	G.addWord(row, col, dictionary.randomWord(), direction)

G.view()

with open("assets/letters.json") as jsonFile:
	letterData = json.load(jsonFile)

G.findAllWords()
