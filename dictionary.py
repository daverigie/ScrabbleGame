import search
import random

dictionaryPath = 'assets/dictionaries/TWL06.txt'

with open (dictionaryPath, "r") as dictFile:
	wordList = dictFile.read().split('\n')

def findWord(word):
	ind, _ = search.binarySearch(wordList, word.upper())
	return ind

def randomWord():
	return random.choice(wordList)

if __name__ == '__main__':
	print(wordList)