LETTER_SPACING = 1

def view(gameBoard):

	tileGrid = gameBoard.tileGrid

	nRows = len(tileGrid)
	nCols = len(tileGrid[0])

	for i in range(nRows):
		for j in range(nCols):
			
			print(tileGrid[i][j], end='')
			
			if j < (nCols-1):
				print(' '*LETTER_SPACING, end='')

		if (i < nRows-1):
			print('\n'*LETTER_SPACING, end='')





	