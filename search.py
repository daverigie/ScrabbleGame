def binarySearch(sortedList, item):

	nItems = len(sortedList)

	left = 0
	right = nItems

	loc = -1
	counter = 0

	while left <= right:
		m = int((left + right)/2)
		
		if item < sortedList[m]:
			right = m - 1
		elif item > sortedList[m]:
			left = m + 1
		else:
			loc = m
			break

		counter = counter + 1

	return loc, counter
	

